package hk.com.novare.springjooq.user.controller;

import hk.com.novare.springjooq.user.model.ClientLoginJsonResponse;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public final class AuthenticationController {
    @ApiOperation("The Process Client Login API is used to authenticate the customer’s login credentials.")
    @PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ClientLoginJsonResponse> login(
        @ApiParam(value = "The user ID of the application accessing the web service.", required = true)
        @RequestHeader("Username")
        final String username,

        @ApiParam(value = "The password of the application accessing the web service.", required = true)
        @RequestHeader("Password")
        final String password,

        @ApiParam(value = "The device name where the application is installed.", required = true)
        @RequestHeader("Device")
        final String device,

        @ApiParam(value = "The IMEI or MAC address of the mobile device.", required = true)
        @RequestHeader("IMEI")
        final String imei,

        @ApiParam(value = "The channel accessing the web service. ", required = true)
        @RequestHeader("Channel")
        final String channel,

        @ApiParam(value = "The IP address of the service provider.", required = true)
        @RequestHeader("MobileIP")
        final String mobileIp,

        @ApiParam(value = "The encrypted user ID of the client.", required = true)
        @RequestParam("Username")
        final String usernameParam,

        @ApiParam(value = "The encrypted password of the client.", required = true)
        @RequestParam("Password")
        final String passwordParam
    ) {
        final ClientLoginJsonResponse response = new ClientLoginJsonResponse(
            UUID.randomUUID().toString(),
            "0",
            "Message",
            "Message",
            UUID.randomUUID().toString(),
            0,
            UUID.randomUUID().toString(),
            "200",
            UUID.randomUUID().toString()
        );
        return new ResponseEntity<>(
            response,
            HttpStatus.OK
        );
    }
}
