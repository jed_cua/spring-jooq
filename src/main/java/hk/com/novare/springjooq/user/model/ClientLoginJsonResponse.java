package hk.com.novare.springjooq.user.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Pattern;

public final class ClientLoginJsonResponse {
    @ApiModelProperty("The feedback message to the client.")
    @Pattern(regexp = "[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89ab][a-f0-9]{3}-[a-f0-9]{12}")
    @JsonProperty("Description")
    private String description;

    @ApiModelProperty("Indicates a code unique to a status.")
    @JsonProperty("ExceptionCode")
    private String exceptionCode;

    @ApiModelProperty(
        "The java exception message, or any other exception "
            + "message that would help in determining what happened to the transaction."
    )
    @JsonProperty("ExceptionMessage")
    private String exceptionMessage;

    @ApiModelProperty(
        "The response code:\n"
            + "1: Proceed with the landing page\n"
            + "0: Error scenario, or other scenarios\n"
    )
    @JsonProperty("ResponseCode")
    private String responseCode;

    @ApiModelProperty("The unique generated ID of the client in TBL_RIB_CLIENT")
    @JsonProperty("ClientId")
    private String clientId;

    @ApiModelProperty(
        "Flag for checking if client's password has expired.\n"
            + "0 – No\n"
            + "1 – Yes\n"
    )
    @JsonProperty("IsPasswordExpired")
    private Integer passwordExpired;

    @ApiModelProperty("The unique generated ID of the TBL_RIB_LOGIN_INFO of the client.")
    @JsonProperty("LoginId")
    private String loginId;

    @ApiModelProperty("Last 6 digits of the login reference number. Required if isOTP is enabled.")
    @JsonProperty("ReferenceNumber")
    private String referenceNumber;

    @ApiModelProperty("The session ID of the login.")
    @JsonProperty("SessionId")
    private String sessionId;

    public ClientLoginJsonResponse(
        final String description,
        final String exceptionCode,
        final String exceptionMessage,
        final String responseCode,
        final String clientId,
        final Integer passwordExpired,
        final String loginId,
        final String referenceNumber,
        final String sessionId
    ) {
        this.description = description;
        this.exceptionCode = exceptionCode;
        this.exceptionMessage = exceptionMessage;
        this.responseCode = responseCode;
        this.clientId = clientId;
        this.passwordExpired = passwordExpired;
        this.loginId = loginId;
        this.referenceNumber = referenceNumber;
        this.sessionId = sessionId;
    }

    public ClientLoginJsonResponse() { }

    public String getDescription() {
        return description;
    }

    public String getExceptionCode() {
        return exceptionCode;
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public String getClientId() {
        return clientId;
    }

    public Integer getPasswordExpired() {
        return passwordExpired;
    }

    public String getLoginId() {
        return loginId;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public void setExceptionCode(final String exceptionCode) {
        this.exceptionCode = exceptionCode;
    }

    public void setExceptionMessage(final String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }

    public void setResponseCode(final String responseCode) {
        this.responseCode = responseCode;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setPasswordExpired(final Integer passwordExpired) {
        this.passwordExpired = passwordExpired;
    }

    public void setLoginId(final String loginId) {
        this.loginId = loginId;
    }

    public void setReferenceNumber(final String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public void setSessionId(final String sessionId) {
        this.sessionId = sessionId;
    }
}
