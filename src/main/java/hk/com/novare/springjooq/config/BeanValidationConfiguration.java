package hk.com.novare.springjooq.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.validation.Validator;

@Configuration
public class BeanValidationConfiguration {
    private final Validator validator;

    @Autowired
    public BeanValidationConfiguration(final Validator validator) {
        this.validator = validator;
    }

    @Bean
    public BeanValidator validator() {
        return new BeanValidator(this.validator);
    }
}
