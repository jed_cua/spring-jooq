package hk.com.novare.springjooq.config;

import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.beanvalidation.SpringValidatorAdapter;

import javax.validation.Validator;

@Component
public final class BeanValidator {
    private final SpringValidatorAdapter validator;

    public BeanValidator(final Validator validator) {
        this.validator = new SpringValidatorAdapter(validator);
    }

    public <T> void validate(final T obj) throws Exception {
        final BindingResult bindingResult = new BeanPropertyBindingResult(obj, "POJO");
        this.validator.validate(obj, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new Exception(
                bindingResult.getAllErrors().toString()
            );
        }
    }
}
