package hk.com.novare.springjooq.user.controller;

import hk.com.novare.springjooq.config.BeanValidator;
import hk.com.novare.springjooq.user.model.ClientLoginJsonResponse;
import io.restassured.RestAssured;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebMvc
@Transactional
public class AuthenticationControllerIntegrationTest {
    @LocalServerPort
    private Integer port;

    @Autowired
    private BeanValidator validator;

    @Before
    public void prepare() {
        RestAssured.port = this.port;
    }

    @Test
    public void login() throws Exception {
        final ClientLoginJsonResponse response = RestAssured
            .given()
            .header("Channel", "qwerty").and()
            .header("Device", "qwerty").and()
            .header("IMEI", "qwerty").and()
            .header("MobileIP", "qwerty").and()
            .header("Password", "qwerty").and()
            .header("Username", "qwerty").and()
            .queryParam("Username", "qwerty").and()
            .queryParam("Password", "qwerty")
            .post("/login")
            .then()
            .extract()
            .body()
            .as(ClientLoginJsonResponse.class);
        this.validator.validate(response);
    }
}